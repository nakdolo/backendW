import { Request, Response, Router } from 'express'
import { User } from '../../domains/user'
import { AuthMiddleware } from '../../middlewares/auth'

export class ProfileController {
    public router: Router

    constructor() {
        this.router = Router()

        this.initRoutes()
    }

    private initRoutes(): void {
        this.router.get('/profile', AuthMiddleware.verifyClientAccessToken, this.getProfile)
    }

    public async getProfile(req: Request, res: Response): Promise<void> {
        const result = await User.getProfile(req.body.userId)

        res.status(200).send(result)
    }
}
