import { IsEmail, IsNotEmpty, IsPhoneNumber, IsString } from 'class-validator'

export class SignupDto {
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    email!: string

    @IsString()
    @IsNotEmpty()
    first_name!: string

    @IsString()
    @IsNotEmpty()
    last_name!: string

    @IsString()
    @IsNotEmpty()
    middle_name!: string

    @IsString()
    @IsNotEmpty()
    @IsPhoneNumber()
    phone_number!: string

    @IsString()
    @IsNotEmpty()
    role!: string
}
