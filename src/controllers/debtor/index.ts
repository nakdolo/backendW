import { Request, Response, Router } from 'express'
import { Debtor } from '../../domains/debtor'

export class DebtorController {
    public router: Router

    constructor() {
        this.router = Router()
        this.initRoutes()
    }

    private initRoutes(): void {
        this.router.get('/debtor', this.paginatedDebtors)
        this.router.get('/debtor/:id', this.debtorById)
    }

    public async debtorById(req: Request, res: Response): Promise<void> {
        const id: number = parseInt(req.params.id)

        const debtor = await Debtor.getDebtorById({ id })
        res.status(200).send(debtor)
    }

    public async paginatedDebtors(req: Request, res: Response): Promise<void> {
        const page: number = parseInt(req.query.page as string, 10) || 1
        const pageSize: number = parseInt(req.query.pageSize as string, 10) || 10

        const searchText: string = req.query.text as string

        const paginatedResult = await Debtor.getPaginatedDebtor({ page, pageSize, searchText })

        res.status(200).send(paginatedResult)
    }
}
