import { AuthController } from './controllers/auth'
import { ProfileController } from './controllers/profile'
import { initDatabase } from './db/init-database'
import { Config } from './domains/config'
import { ServerApp } from './domains/server-app'
import { DebtorController } from './controllers/debtor'

const app = new ServerApp([new AuthController(), new ProfileController(), new  DebtorController()], Config.clientApiPort)

const init = async (): Promise<void> => {
    app.init()
}

initDatabase().then(init)
