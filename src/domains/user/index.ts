import { UserData } from '../../data/user'
import { UnauthorizedException } from '../exception'

export type ProfileType = {
    id: string | number
    name: string
    surname: string
    phoneNumber: string | null
}

export class User {
    public static async getProfile(userId: string): Promise<ProfileType> {
        const user = await UserData.getUserById(userId)

        if (!user) {
            throw new UnauthorizedException()
        }

        return {
            id: user.id,
            name: user.name,
            surname: user.surname,
            phoneNumber: user.phoneNumber,
        }
    }
}
