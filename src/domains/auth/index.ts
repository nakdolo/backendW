import { LoginDto } from '../../controllers/auth/dto/login.dto'
import { SignupDto } from '../../controllers/auth/dto/signup.dto'
import { UserData } from '../../data/user'
import { BadRequestException } from '../exception'
import { Token, TokenType } from '../token'

export type AuthResultType = {
    token: TokenType
    department: string
    role: string
}

export class Auth {
    public static async login(data: LoginDto): Promise<AuthResultType> {
        const user = await UserData.getUserByEmail(data.email)

        if (!user) {
            throw new BadRequestException('Неверные учетные данные')
        }

        if (user.password !== data.password) {
            throw new BadRequestException('Неверные учетные данные')
        }

        const token = Token.signTokenForUser(
            {
                id: user.id,
            },
            {
                expiresIn: data.remeberMe ? '1h' : '6h',
            },
        )

        return {
            token,
            department: 'css',
            role: 'css',
        }
    }

    public static async signup(data: SignupDto): Promise<void> {
        const userByEmail = await UserData.getUserByEmail(data.email)
        if (userByEmail) {
            throw new BadRequestException()
        }
        const userByPhoneNumber = await UserData.getUserByPhoneNumber(data.phone_number)
        if (userByPhoneNumber) {
            throw new BadRequestException()
        }
    }
}
