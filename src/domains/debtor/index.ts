import { DebtorData } from '../../data/debtor'
import { NotFoundException } from '../../domains/exception'

export type DebtorType = {
    id: number
    district: string
    group: string
    typeOfConsumer: string
    personalAccount: number
    name: string
    amountOfPayments: number
    theBeginningOfTheCalculation: string
    totalDebt: number
}

export type DebtorSearchOptions = {
    page: number
    pageSize: number
    searchText: string
}

export type DebtorSearchById = {
    id: number
}

export class Debtor {
    public static async getDebtorById(options: DebtorSearchById): Promise<DebtorType | undefined> {
        const debtor = await DebtorData.getDebterById(options.id)
        if (debtor == null) {
            throw new NotFoundException()
        }
        return debtor
    }

    public static async getPaginatedDebtor(options: DebtorSearchOptions): Promise<DebtorType[]> {
        const debtors = await DebtorData.getPaginatedDebtorInformation(
            options.page,
            options.pageSize,
            options.searchText,
        )
        if (debtors.length === 0) {
            throw new NotFoundException()
        }
        return debtors
    }
}
