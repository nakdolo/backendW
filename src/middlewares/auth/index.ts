import { plainToClass } from 'class-transformer'
import { validate } from 'class-validator'
import { NextFunction, Request, Response } from 'express'
import { verify } from 'jsonwebtoken'
import { LoginDto } from '../../controllers/auth/dto/login.dto'
import { Config } from '../../domains/config'
import {
    BadRequestException,
    InternalServerErrorException,
    UnauthorizedException,
} from '../../domains/exception'
import { Utils } from '../../utils'

export class AuthMiddleware {
    public static async verifyClientAccessToken(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<void> {
        try {
            const headers = req.headers.authorization || ''
            const arr = headers.split(' ')

            if (arr.length !== 2) {
                throw new UnauthorizedException()
            }

            const tokenType = arr[0]
            const token = arr[1]

            if (tokenType !== 'Bearer') {
                throw new UnauthorizedException()
            }

            try {
                const verifyResult: any = verify(token, Config.clientAuthSecretOrPrivateKey)

                console.log(verifyResult)

                req.body.userId = verifyResult.id

                return next()
            } catch (e) {
                console.log(e)
                throw new UnauthorizedException()
            }
        } catch (e) {
            console.log(e)
            throw new InternalServerErrorException()
        }
    }

    public static async verifyLoginData(req: Request, res: Response, next: NextFunction): Promise<void> {
        const dto: LoginDto = plainToClass(LoginDto, req.body)

        const errorTexts: string[] = Utils.getErrorMessages(await validate(dto))

        if (errorTexts.length > 0) {
            throw new BadRequestException(errorTexts)
        }

        next()
    }
}
