export type UserType = {
    id: number
    email: string
    password: string
    phoneNumber: string | null
    name: string
    surname: string
}

const users: UserType[] = [
    {
        id: 1,
        email: 'test@test.com',
        phoneNumber: null,
        password: 'testpass123',
        name: 'Test Name',
        surname: 'Test Surname',
    },
]

export class UserData {
    public static async getUserById(userId: string | number): Promise<UserType | null> {
        const foundUser = users.find((v) => v.id.toString() === userId.toString())

        if (foundUser) {
            return foundUser
        }

        return null
    }

    public static async getUserByEmail(email: string): Promise<UserType | null> {
        const foundUser = users.find((v) => v.email === email)

        if (foundUser) {
            return foundUser
        }

        return null
    }

    public static async getUserByPhoneNumber(phoneNumber: string): Promise<UserType | null> {
        const foundUser = users.find((v) => v.phoneNumber === phoneNumber)

        if (foundUser) {
            return foundUser
        }

        return null
    }
}
