import { Pool, QueryResult } from 'pg'

export type DebtorType = {
    id: number
    district: string
    group: string
    typeOfConsumer: string
    personalAccount: number
    name: string
    amountOfPayments: number
    theBeginningOfTheCalculation: string
    totalDebt: number
}

const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    port: 5432,
    password: 'asdfg',
    database: 'DebtorDB',
})

export class DebtorData {
    public static async getDebterById(debtorId: string | number): Promise<DebtorType | undefined> {
        const client = await pool.connect()

        try {
            const res: QueryResult<DebtorType> = await client.query<DebtorType>(
                'SELECT * FROM debtor WHERE ID = $1',
                [debtorId],
            )
            const debtor = res.rows[0]

            return debtor
        } catch (error) {
            console.log('Ошибка', error)
            throw new Error('Не удалось получить информацию о должнике.')
        } finally {
            if (client) {
                client.release()
            }
        }
    }

    public static async getPaginatedDebtorInformation(
        page: number,
        pageSize: number,
        searchText?: string,
    ): Promise<DebtorType[]> {
        const client = await pool.connect()

        let filteredDebtors: DebtorType[] = []

        try {
            if (!searchText || searchText.trim() === '') {
                const res: QueryResult<DebtorType> = await client.query<DebtorType>('SELECT * FROM debtor')
                filteredDebtors = res.rows
            } else {
                const trimmedSearchText = searchText.trim().toLowerCase()
                const queryText = `SELECT * FROM debtor 
                                    WHERE LOWER("group"::text) = $1 OR LOWER("district"::text) = $1`

                const res: QueryResult<DebtorType> = await client.query<DebtorType>(queryText, [
                    trimmedSearchText,
                ])

                filteredDebtors = res.rows
            }

            const startI = page - 1
            const endI = startI + pageSize

            const paginatedDebtors = filteredDebtors.slice(startI, endI)

            return paginatedDebtors
        } catch (error) {
            console.log('Ошибка', error)
            throw new Error('Не удалось получить информацию о должниках.')
        } finally {
            client.release()
        }
    }
}
